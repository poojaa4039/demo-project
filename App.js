import React, { Component } from "react";
import "./App.css";
import Navbar from "./components/navbar";
import { Route, Switch, Redirect } from "react-router-dom";
import Players from "./components/players";
import http from "./service/httpService";
import config from "./config.json";
import DetailsComp from "./components/details";
class App extends Component {
  state = {
    stars: [],
  };

  async componentDidMount() {
    const { data: stars } = await http.get(config.accessedURL);
    this.setState({ stars });
  }
  render() {
    return (
      <div>
        <Navbar />
        <div className="content">
          <Switch>
            <Route
              path="/stars/:sport"
              render={(props) => <Players data={this.state.stars} {...props} />}
            />
            <Route
              path="/details/:id"
              render={(props) => (
                <DetailsComp data={this.state.stars} {...props} />
              )}
            />
            <Route
              path="/stars"
              render={(props) => <Players data={this.state.stars} {...props} />}
            />
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
