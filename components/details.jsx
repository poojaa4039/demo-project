import React, { Component } from "react";
import { Link } from "react-router-dom";

class DetailsComp extends Component {
  state = { foundStar: {} };
  handleFound = () => {
    let stars = this.props.data;
    let id = this.props.match.params.id;
    let foundStar = stars.find((st) => st.id === id);
    console.log("id", foundStar);
    return foundStar;
  };
  render() {
    let foundStar1 = this.handleFound();
    let foundStar = { ...foundStar1 };

    return (
      <div className="container padding">
        <h3>{foundStar.name} </h3>

        <h5>Date of Birth: {foundStar.details.DOb}</h5>
        <h5>Sport: {foundStar.sport}</h5>
        <h5>Country: {foundStar.country}</h5>
        {foundStar.details.Info}

        <Link className="nav-link" to={`/stars/${foundStar.sport}`}>
          {foundStar.sport} Stars
        </Link>
      </div>
    );
  }
}

export default DetailsComp;
