import React from "react";
import { Link } from "react-router-dom";
const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm bg-dark navbar-dark">
      <Link className="navbar-brand" to="/stars">
        Sport Stars{" "}
      </Link>
      <div className="" id="nav-main">
        <ul className="navbar-nav mr-auto">
          <li className="nav-item">
            <Link className="nav-link" to="/stars">
              All
            </Link>
          </li>{" "}
          <li className="nav-item">
            <Link className="nav-link" to="/stars/Cricket">
              Cricket
            </Link>
          </li>
          <li className="nav-item">
            <Link className="nav-link" to="/stars/Football">
              Football
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
