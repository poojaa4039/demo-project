import React, { Component } from "react";
import { Link } from "react-router-dom";

class Players extends Component {
  filteredData = (selectedSport) => {
    let data = this.props.data;
    data = data.filter((star) => star.sport === selectedSport);
    return data;
  };
  render() {
    let data = this.props.data;
    let selectedSport = this.props.match.params.sport;
    data =
      selectedSport === undefined ? data : this.filteredData(selectedSport);
    return (
      <div className="container mt-4">
        <div className="row text-center">
          <div className="col-3 border  bg-primary ">Name</div>
          <div className="col-3 border  bg-primary ">Country</div>
          <div className="col-3 border  bg-primary ">Sport</div>
        </div>
        {data.map((st) => (
          <div className="row text-center" key={st.id}>
            <div className="col-3 border">
              <Link to={`/details/${st.id}`}>{st.name}</Link>
            </div>
            <div className="col-3 border">{st.country}</div>
            <div className="col-3 border">{st.sport}</div>
          </div>
        ))}
      </div>
    );
  }
}

export default Players;
